﻿# Create AES Key File
$KeyFile = "\\path\to\keyfile\AES.key"
$Key = New-Object Byte[] 16   # You can use 16, 24, or 32 for AES
[Security.Cryptography.RNGCryptoServiceProvider]::Create().GetBytes($Key)
$Key | out-file $KeyFile

# Create Secure Credential File using AES Key File
$PasswordFile = "\\path\to\passwordfile\PW.txt"
$Key = Get-Content $KeyFile
$Password = "" | ConvertTo-SecureString -AsPlainText -Force
$Password | ConvertFrom-SecureString -key $Key | Out-File $PasswordFile


$User = "username@tenant.onmicrosoft.com"
$PasswordFile = "\\path\to\passwordfile\PW.txt"
$KeyFile = "\\path\to\keyfile\AES.key"
$key = Get-Content $KeyFile
$MyCredential = New-Object -TypeName System.Management.Automation.PSCredential `
 -ArgumentList $User, (Get-Content $PasswordFile | ConvertTo-SecureString -Key $key)
